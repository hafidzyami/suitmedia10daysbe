<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class BackupDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup the database';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Get database configuration
        $dbHost = "127.0.0.1";
        $dbPort = "3306";
        $dbName = "suitmedia10days";
        $dbUser = "root";
        $dbPass = "";
        
        // Define the filename and path for the backup file
        $backupPath = storage_path('app/backups');
        $filename = 'backup_' . date('Ymd_His') . '.sql';
        $backupFile = $backupPath . '/' . $filename;

        // Ensure the backup directory exists
        if (!is_dir($backupPath)) {
            mkdir($backupPath, 0755, true);
        }

        // Perform the database backup using mysqldump
        $command = sprintf(
            'mysqldump --user=%s --password=%s --host=%s --port=%s %s > %s',
            escapeshellarg($dbUser),
            escapeshellarg($dbPass),
            escapeshellarg($dbHost),
            escapeshellarg($dbPort),
            escapeshellarg($dbName),
            escapeshellarg($backupFile)
        );

        $result = null;
        $output = null;
        exec($command, $output, $result);

        // Check if the backup was successful
        if ($result === 0) {
            $this->info("Database backup was successful. Backup file created at: {$backupFile}");
        } else {
            $this->error("Database backup failed.");
            $this->error("Command executed: {$command}");
            $this->error("Result: {$result}");
            $this->error("Output: " . implode("\n", $output));
        }

        return $result;
    }
}
