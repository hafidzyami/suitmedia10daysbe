<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Post;
use App\Services\TranslationService;
use Illuminate\Support\Facades\Cache;

class ModifyPostContent extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:modify {word} {replacement} {post_id} {locale}';
    protected $translationService;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Modify the content of a specific post by replacing a specific word with another word';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TranslationService $translationService)
    {
        parent::__construct();
        $this->translationService = $translationService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $word = $this->argument('word');
        $replacement = $this->argument('replacement');
        $postId = $this->argument('post_id');


        $post = Post::find($postId);


        if (!$post) {
            $this->info("Post with ID {$postId} not found.");
            return 0;
        }

        $originalBody = $this->translationService->translate($post->body, $this->argument('locale'));
        $modifiedBody = str_replace($word, $replacement, $originalBody);

        if ($originalBody !== $modifiedBody) {
            $post->body = $modifiedBody;
            $post->save();

            // update the cache
            $locales = ['en', 'id'];
            foreach ($locales as $locale) {
                $cacheKey = "posts_{$locale}";

                if (Cache::has($cacheKey)) {
                    $cachedPosts = Cache::get($cacheKey);
                    // Update the specific post in the cached collection
                    if (isset($cachedPosts[($post->id - 1)])) {
                        $cachedPosts[($post->id - 1)]->body = $this->translationService->translate($modifiedBody, $locale);
                        Cache::put($cacheKey, $cachedPosts, 3600);
                        $this->info("Cache for locale '{$locale}' updated.");
                    }
                } else {
                    $this->info("Cache key '{$cacheKey}' not found.");
                }
            }

            $this->info("Modified post ID {$post->id}: replaced '{$word}' with '{$replacement}'");
        } else {
            $this->info("No changes made to post ID {$post->id}.");
        }

        return 0;
    }
}
