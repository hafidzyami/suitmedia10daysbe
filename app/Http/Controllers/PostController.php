<?php

namespace App\Http\Controllers;

use App\Jobs\SendPostSubmittedEmail;
use App\Services\TranslationService;
use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Exports\PostsExport;
use Maatwebsite\Excel\Facades\Excel;

class PostController extends Controller
{
    protected $translationService;

    public function __construct(TranslationService $translationService)
    {
        $this->translationService = $translationService;
    }

    public function export()
    {
        return Excel::download(new PostsExport, 'posts.xlsx');
    }

    public function index()
    {
        $locale = app()->getLocale();
        $cacheKey = "posts_{$locale}";

        if (Cache::has($cacheKey)) {
            $posts = Cache::get($cacheKey);
        } else {
        
            $posts = Post::all();
            foreach ($posts as $post) {
                $post->title = $this->translationService->translate($post->title, $locale);
                $post->body = $this->translationService->translate($post->body, $locale);
            }
            Cache::put($cacheKey, $posts, 3600);
        }

        return view('posts.index', compact('posts'));
    }

    public function create()
    {
        // Menampilkan formulir pembuatan post
        return view('posts.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'body' => 'required',
            'image' => 'required',
        ]);

        try {
            $title = $request->input('title');
            $body = $request->input('body');

            $titleTranslated = $this->translationService->translate($title, app()->getLocale());
            $bodyTranslated = $this->translationService->translate($body, app()->getLocale());

            $post = new Post([
                'title' => $titleTranslated,
                'body' => $bodyTranslated,
            ]);

            $post->user()->associate(Auth::user());
            $post->save();

            if ($request->hasFile('image')) {
                $media = $post->addMediaFromRequest('image')->toMediaCollection('images');
                dd($media);
            }
            else{
                dd('no image');
            }

            $locales = ['en', 'id'];
            foreach ($locales as $locale) {
                $cacheKey = "posts_{$locale}";
                $posts = Cache::get($cacheKey, []);
                $translatedPost = clone $post;
                $translatedPost->title = $this->translationService->translate($post->title, $locale);
                $translatedPost->body = $this->translationService->translate($post->body, $locale);
                $posts[] = $translatedPost;
                Cache::put($cacheKey, $posts, 3600); // Cache selama 1 jam
            }
            
            // Dispatch
            SendPostSubmittedEmail::dispatch($post);

            session()->flash('success', __('posts.created_successfully'));
        } catch (\Exception $e) {
            session()->flash('error', __('posts.failed_to_create'));
        }

        return redirect()->route('posts.index');
    }
}