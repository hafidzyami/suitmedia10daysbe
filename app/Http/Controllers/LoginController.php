<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            session()->flash('success', 'Login berhasil!');
            return redirect()->intended('/posts');
        }else{
            session()->flash('error', 'Login gagal!');
            return redirect()->intended('/login');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/posts');
    }
}
