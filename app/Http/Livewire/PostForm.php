<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Jobs\SendPostSubmittedEmail;
use App\Services\TranslationService;
use Livewire\WithFileUploads;
use Intervention\Image\ImageManagerStatic as Image;

class PostForm extends Component
{
    use WithFileUploads;

    public $title;
    public $body;
    public $photo;
    public $imageUploadStatus;

    protected $rules = [
        'title' => 'required|max:255',
        'body' => 'required',
        'photo' => 'required|image|max:2048'
    ];

    protected $translationService;

    public function mount(TranslationService $translationService)
    {
        $this->translationService = $translationService;
    }

    public function submit()
    {
        $this->validate();

        try {
            $locale = app()->getLocale();

            $post = new Post([
                'title' => $this->title,
                'body' => $this->body,
            ]);

            if ($this->photo) {
                $path = $this->photo->store('public/posts');
                $image = Image::make(storage_path('app/' . $path))->resize(800, 600);
                $image->save();
                $post->addMedia(storage_path('app/' . $path))->toMediaCollection('images');
            } else {
                dd('Image is required');
            }

            $post->user()->associate(Auth::user());
            $post->save();

            $locales = ['en', 'id'];
            foreach ($locales as $locale) {
                $cacheKey = "posts_{$locale}";
                $posts = Cache::get($cacheKey, []);
                $translatedPost = clone $post;
                $posts[] = $translatedPost;
                Cache::put($cacheKey, $posts, 3600); // Cache for 1 hour
            }

            // Dispatch job
            SendPostSubmittedEmail::dispatch($post);

            session()->flash('success', __('posts.created_successfully'));

            $this->reset(['title', 'body', 'photo']);
            $this->imageUploadStatus = 'Image uploaded successfully';
        } catch (\Exception $e) {
            dd($e->getMessage());
            session()->flash('error', __('posts.failed_to_create'));
        }
        return redirect()->route('posts.index');
    }

    public function render()
    {
        return view('livewire.post-form');
    }
}

?>