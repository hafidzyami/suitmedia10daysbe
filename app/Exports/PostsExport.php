<?php

namespace App\Exports;

use App\Models\Post;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PostsExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Post::with('media')->get();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Title',
            'Body',
            'Image URL',
            'Created At',
            'Updated At',
        ];
    }

    public function map($post): array
    {
        return [
            $post->id,
            $post->title,
            $post->body,
            $post->getFirstMediaUrl('images'),
            $post->created_at,
            $post->updated_at,
        ];
    }
}
