<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\PostController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Mail;
use App\Models\Post;
use App\Mail\PostSubmitted;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::post('login', [LoginController::class, 'login']);
// Route::post('/logout', [LoginController::class, 'logout'])->name('logout');


Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [AuthController::class, 'login']);
Route::post('logout', [AuthController::class, 'logout'])->middleware('auth:sanctum')->name('logout');

Route::get('auth/google', [AuthController::class, 'redirectToGoogle']);
Route::get('auth/google/callback', [AuthController::class, 'handleGoogleCallback']);



Route::get('/posts', [PostController::class, 'index'])->name('posts.index');
Route::get('/posts/create', [PostController::class, 'create'])->middleware('auth:sanctum')->name('posts.create');
Route::post('/posts', [PostController::class, 'store'])->middleware('auth:sanctum');

Route::get('lang/{locale}', function ($locale) {
    if (in_array($locale, ['en', 'id'])) {
        Session::put('locale', $locale);
    }
    return redirect()->back();
})->name('setLocale');

// Route::resource('users', UserController::class);
// Route::resource('posts', PostController::class)->middleware('guest');

Route::get('/run-seeder', function () {
    Artisan::call('db:seed');
    return 'Seeder has been run';
});

Route::get('/check-cache', function () {
    // Check the default cache driver
    $defaultDriver = Config::get('cache.default');
    echo "Default Cache Driver: " . $defaultDriver . "<br>";

    // Check if the default driver is Redis
    if ($defaultDriver === 'redis') {
        echo "Redis is configured as the default cache driver.<br>";
    }

    // Test setting and getting a cache value

    // List all keys in Redis
    $keys = Redis::keys('*');
    echo "Cached Keys: <br>";
    foreach ($keys as $key) {
        echo $key . "<br>";
    }
});

Route::get('/test-mail', function () {
    try {
        Mail::raw('This is a test email.', function ($message) {
            $message->to('hafidzshidqi69@gmail.com')
                    ->subject('Test Email');
        });
        return 'Email sent successfully!';
    } catch (\Exception $e) {
        return 'Failed to send email: ' . $e->getMessage();
    }
});


Route::get('/test-mail2', function () {
    try {
        $post = Post::first(); // Get the first post or create a dummy one for testing
        Mail::to('hafidzshidqi69@gmail.com')->send(new PostSubmitted($post));
        return 'Email sent successfully!';
    } catch (\Exception $e) {
        return 'Failed to send email: ' . $e->getMessage();
    }
});

Route::get('/posts/export', [PostController::class, 'export'])->name('posts.export');