<?php

return [
    'created_successfully' => 'Post created successfully.',
    'failed_to_create' => 'Failed to create post.',
    'list_of_posts' => 'List of Posts',
    'title' => 'Title',
    'content'  => 'Content',
    'create_post' => 'Create Post',
    'user_id' => 'User ID',
];