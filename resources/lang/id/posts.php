<?php

return [
    'created_successfully' => 'Post berhasil dibuat.',
    'failed_to_create' => 'Gagal membuat post.',
    'list_of_posts' => 'Daftar Post',
    'title' => 'Judul',
    'content'  => 'Konten',
    'create_post' => 'Buat Post',
    'user_id' => 'ID Pengguna',
];