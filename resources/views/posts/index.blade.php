@extends('layouts.app')

@section('content')
    <h1>{{ __('posts.list_of_posts') }}</h1>
    <a class="dropdown-item" href="{{ route('posts.export') }}">Export to Excel</a>
    <ul>
        @foreach($posts as $post)
            <li>
                <strong>{{ __('ID') }}:</strong> {{ $post->id }}<br>
                <strong>{{ __('posts.title') }}:</strong> {{ $post->title }}<br>
                <strong>{{ __('posts.content') }}:</strong> {{ $post->body }}<br>
                <strong>{{ __('posts.user_id') }}:</strong> {{ $post->user_id }}<br>
                @if($post->getFirstMediaUrl('images', 'thumb'))
    <?php
    $imageUrl = str_replace('/storage/', '/suitmedia10daysbe/public/storage/', $post->getFirstMediaUrl('images', 'thumb'));
    ?>
    <p>Image URL: <a href="{{ $imageUrl }}" target="_blank">{{ $imageUrl }}</a></p>
    <img src="{{ $imageUrl }}" alt="{{ $post->title }}">
@else
@endif


                <hr>
            </li>
        @endforeach
    </ul>

    <a href="{{ route('posts.create') }}">{{ __('posts.create_post') }}</a>
@endsection
