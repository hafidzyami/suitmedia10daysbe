@extends('layouts.app')

@section('content')
    <h1>{{ __('posts.create_post') }}</h1>

    <livewire:post-form />
@endsection

{{-- @section('content')
    <h1>{{ __('posts.create_post') }}</h1>

    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <form action="/posts" method="POST">
        @csrf
        <div>
            <label for="title">{{ __('posts.title') }}</label>
            <input type="text" id="title" name="title" value="{{ old('title') }}">
            @error('title')
                <div style="color: red">{{ $message }}</div>
            @enderror
        </div>
        <div>
            <label for="body">{{ __('posts.content') }}</label>
            <textarea id="body" name="body">{{ old('body') }}</textarea>
            @error('body')
                <div style="color: red">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit">Submit</button>
    </form>
@endsection --}}
