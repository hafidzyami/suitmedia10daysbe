<div>
    <form wire:submit.prevent="submit">
        @csrf
        <div class="form-group">
            <label for="title">{{ __('posts.title') }}</label>
            <input type="text" id="title" wire:model="title" class="form-control">
            @error('title') <span class="text-danger">{{ $message }}</span> @enderror
        </div>

        <div class="form-group">
            <label for="body">{{ __('posts.content') }}</label>
            <textarea id="body" wire:model="body" class="form-control"></textarea>
            @error('body') <span class="text-danger">{{ $message }}</span> @enderror
        </div>

        @if ($photo)
        Photo Preview:
        <img src="{{ $photo->temporaryUrl() }}">
    @endif
 
    <input type="file" wire:model="photo">
 
    @error('photo') <span class="error">{{ $message }}</span> @enderror
        
        

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
