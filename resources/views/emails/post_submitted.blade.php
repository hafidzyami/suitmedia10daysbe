<!DOCTYPE html>
<html>
<head>
    <title>New Post Submitted</title>
</head>
<body>
    <h1>New Post Submitted</h1>
    <p>Title: {{ $post->title }}</p>
    <p>Body: {{ $post->body }}</p>
</body>
</html>
